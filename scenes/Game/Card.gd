extends Area2D

var pressed = false
var enter = false
var pressed_motion = false
var enter_count = 0
var press_count = 0

var mpos
var regex = RegEx.new()
var card_name
var area_name
var deck_name = ""
var is_back_showing = true

var area_enter = false
var view_size

var DEBUG = true

func _ready():
	set_fixed_process(true)
	set_process_input(true)
	view_size = get_viewport().get_rect().size

	connect("mouse_enter", self, "_mouse_enter")
	connect("mouse_exit", self, "_mouse_exit")
	connect("area_enter", self, "_on_Area2D_area_enter")
	connect("area_exit", self, "_on_Area2D_area_exit")

func _fixed_process(delta):
	view_size = get_viewport().get_rect().size
	var visible = true
	if pressed_motion:
		#print("is visible: " + str(visible))
		for o in get_overlapping_areas():
			if o.get_z() > self.get_z():
				print("o " + str(o.get_name()))
				visible = false
			else:
				visible = true
		if visible:
			self.set_pos(mpos)
			for p_id in gamestate.players:
				rpc_id(p_id, "remote_card_pos", mpos)


func _input_event(viewport, ev, shape_idx):
	if ev.type == InputEvent.MOUSE_BUTTON and ev.button_index == BUTTON_LEFT:
		pressed = ev.pressed
		set_z(get_z()+1)
	
	if ev.type == InputEvent.MOUSE_BUTTON and ev.button_index == BUTTON_RIGHT and ev.is_pressed():
		if is_back_showing:
			get_node("SpriteBack").hide()
			is_back_showing = false
		else:
			get_node("SpriteBack").show()
			is_back_showing = true
	
	if ev.type == InputEvent.MOUSE_MOTION:
		if pressed:
			pressed_motion = true
			mpos = ev.pos
		else:
			pressed_motion = false

remote func remote_card_pos(pos):
	regex.compile("([a-zA-Z_0-9 \\.\\(\\)])+") 
	regex.find(self.get_name())
	card_name = regex.get_capture(0)
	
	get_parent().get_node(card_name).set_pos(pos)
	
func _mouse_enter():
    enter = true

func _mouse_exit():
	if pressed == true:
		pressed = false
	enter = false

func _on_Area2D_area_enter(area ):
	area_enter = true
	regex.compile("([a-zA-Z_0-9 \\.\\(\\)])+") 
	regex.find(self.get_name())
	card_name = regex.get_capture(0)
	
	regex.find(area.get_name())
	area_name = regex.get_capture(0)
		
	if gamepack.cards_in_play[card_name] == area_name and enter_count >= gamestate.get_player_list().size()+1:
		if DEBUG:
			print("replacing card")
		gamepack.replace_card(card_name)
		get_parent().remove_child(self)

		for c in gamepack.cards_in_play:
			print("Card in play: " + c)
		
		enter_count = 0
	
	enter_count += 1

func _on_Area2D_area_exit(area):
	area_enter = false
	
	

