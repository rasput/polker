extends Node2D

var base = "res://GamePack/" 
var game = gamepack.get_current_game_name()

var deck  # Cards and Decks
var stack # Die and other small pieces
var deck_position = []
var stack_position = []

var DEBUG = true

func _ready():
	if DEBUG:
		print("Game ready")
		
	set_fixed_process(true)
	game = gamepack.get_current_game_name()
	rpc("build_tree")

func _fixed_process(delta):
	for n in get_children():
		if n.is_type("Area2D"):
			for o in n.get_overlapping_areas():
				print(n.get_name() + " : " + o.get_name() + " : " + str(o))
	

sync func build_tree():
	if DEBUG:
		print("Building game tree")
		
	var deck_count = 0
	var stack_count = 0
	
	for pos in get_node("Decks").get_children():
		deck_position.append(pos.get_pos())
	for pos in get_node("Stacks").get_children():
		stack_position.append(pos.get_pos())

	#Create the stacks and decks on the game board
	for obj in gamepack.get_object_list(game, "res"):
		if DEBUG:
			print("Game : " + game )
			print("Adding obj : " + obj)
		if obj == "Decks":
			for inst in gamepack.get_instance_list(game, obj, "res"):
				if DEBUG:
					print("Adding game object instance: " + inst)
				add_deck(deck_count, inst, base + "/" + game + "/" + obj + "/" + inst + "/" + "Back.png")
				deck_count = deck_count + 1
				
		if obj == "Pieces" or obj == "2_sided_Pieces":
			for inst in gamepack.get_instance_list(game, obj,"res"):
				add_stack(stack_count, inst, base + "/" + game + "/" + obj + "/" + inst + "/" + "Back.png")
				stack_count = stack_count + 1
		
		if obj == "Die":
			print("Die not implemented!")

func add_deck(idx, name, image):
	if DEBUG:
		print("Adding Deck Name: " + name)
	var deck = load("res://scenes/Game/Deck.tscn").instance() 
	self.add_child(deck)
	deck.set_name(name)
	deck.get_node("Sprite").set_texture(load(image)) #"res://GamePack/Illuminati/Decks/Group_Cards/Back.png"
	deck.set_pos(deck_position[idx])

func add_stack(idx, name, image):
	var stack = load("res://scenes/Game/Stack.tscn").instance() # Programattically generating the shape isn't working
	self.add_child(stack)
	stack.set_name(name)
	stack.get_node("Sprite").set_texture(load(image)) #"res://GamePack/Illuminati/Decks/Group_Cards/Back.png"
	stack.set_pos(stack_position[idx])
	if DEBUG:
		print(stack_position[idx])


