extends Area2D

var base = "res://GamePack/" 
onready var game = gamepack.get_current_game_name()

var DEBUG = true
var click = false
var regex = RegEx.new()
var piece_drawn = ""
var _stack_name = ""

func _ready():
	gamepack.connect("piece_drawn",self,"_piece_drawn")
	set_fixed_process(true)
	
func _fixed_process(delta):
	pass


func _input_event(viewport, ev, shape_idx):
	if ev.type == InputEvent.MOUSE_BUTTON and ev.button_index == BUTTON_RIGHT and ev.pressed == true and ev.is_pressed():
		if DEBUG:
			print("Stack right clicked")
		click = true
		add_piece_from_stack(get_name())

func add_piece_from_stack(stack_name):
	print("Adding from stack : " + str(stack_name))
	regex.compile("([a-zA-Z_0-9\\ \\.\\(\\)])+") 
	regex.find(stack_name)
	stack_name = regex.get_capture(0)
	_stack_name = stack_name
	if DEBUG:
		print("Drawing piece from stack " + stack_name)
		
	gamepack.draw_piece(stack_name)
	
func _piece_drawn():
	if click:
		if DEBUG:
			print("Piece drawn on " + str(get_name()))
		piece_drawn = gamepack.get_piece()
		if DEBUG:
			print("piece_drawn: " + piece_drawn)
		if(piece_drawn != "NoMorePieces"):
			var piece = load("res://scenes/Game/Piece.tscn").instance() 
			piece.set_name(piece_drawn)
			
			self.get_parent().add_child(piece)
			piece.set_pos(self.get_pos())
			piece.stack_name = _stack_name
			piece.get_node("SpriteBack").set_texture(load(base + game +"/Pieces/"+ _stack_name +"/Back.png")) 
			piece.get_node("Sprite").set_texture(load(base + game +"/Pieces/"+ _stack_name +"/"+ piece_drawn)) 
			for p_id in gamestate.players:
				rpc_id(p_id, "deal_piece", _stack_name, piece_drawn)

	click = false
