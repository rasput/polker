# polker

## Description
2D multiplayer card and board game simulator.
Features include:
1. Importing custom games created by players
2. Lan or online play, application is server and client, no internet required.
3. Chat lobby
3. Finished simulator will support:
* Multiple Decks and their cards
* Multiple stacks of different or same game pieces
* Dealing and replacing cards to and from their decks
* Dealing and replacing pieces to and from their stacks
* Rolling dice with as many sides as required
* Will come with default game pack
* See where other player's cursor is on screen, name/colour will indicate player

## Structure of game pack:

 GameName(Folder) "Can be any name, will be used for the game name"

 -> version.txt # contains "v0.1" without the quotation marks on first line and any other info

 -> Decks (Folder)

 -> Decks  -> Deck_nnn (Folder)

 -> Decks  -> Deck_nnn -> Back.png

 -> Decks  -> Deck_nnn -> Card_nnn.png


 -> Die (Folder) # Dice faces are counted so keep seperate from pieces

 -> Die    -> Dice_nnn (Folder)

 -> Die    -> Dice_nnn -> Face_nnn.png # Geometry 64x64 # nn corresponds to face number


 -> 2_sided_Pieces (Folder) # 2 sided pieces where you need different back images per piece

 -> 2_sided_Pieces -> Piece_nnn (Folder) # Geometry 64x64

 -> 2_sided_Pieces -> Piece_nnn -> Piece_front_nn.png # nn matches front to back images 

 -> 2_sided_Pieces -> Piece_nnn -> Piece_back_nn.png


 -> Pieces (Folder) # pawns, place holders, chips, credits, etc.

 -> Pieces -> Piece_nnn (Folder)

 -> Pieces -> Piece_nnn -> Piece_nn.png


 -> Mats (Folder)
 -> Mats -> Mat_nnn -> Mat.png

 -> Background.png

## Image Dimensions:
Tranparency can be used
512x256 for Decks and Card
64x64 for 2_Sided_Pieces, Pieces and Die

Mats and Background have yet to be decided upon.

# Using and Testing Game Packs
The application on start up creates GamePack folder in your appdata folder.
On linux for example:
~/.godot/app_userdata/polker/GamePack/

Unzip the game pack folder into this folder

Working:
* Game pack importing
* Online lobby
* Game init
* Stack placing
* Random card dealing
* Game keeps track of what cards are in play
* Replacing cards back in a deck

Known Issues:
* The game pack "importer" is case sensitive
* Dragging cards is slow

Next up:
* Tying the network to the game logic
* Adding mats import and display.
* Adding sided pieces import, logic and display
* Adding die logic and display
* Adding Background image import
* Theming the GUI
* Flipping of cards and pieces logic
* Hidden screen area
* Adding loading screen animations
* Adding animations to card/piece flipping/dealing/replacing to deck/stack