extends Area2D

var pressed = false
var enter = false
var enter_count = 0
var press_count = 0

var regex = RegEx.new()
var piece_name
var area_name
var stack_name = ""
var is_back_showing = true


func _ready():
	set_fixed_process(true)
	set_process_input(true)
	#self.
	connect("mouse_enter", self, "_mouse_enter")
	connect("mouse_exit", self, "_mouse_exit")
	connect("area_enter", self, "_on_Area2D_area_enter")
	connect("area_exit", self, "_on_Area2D_area_exit")


func _input_event(viewport, ev, shape_idx):
	if ev.type == InputEvent.MOUSE_BUTTON and ev.button_index == BUTTON_LEFT:
		print("suprise")
		pressed = ev.pressed

	if ev.type == InputEvent.MOUSE_BUTTON and ev.button_index == BUTTON_RIGHT and ev.is_pressed():
		if is_back_showing:
			get_node("SpriteBack").hide()
			is_back_showing = false
		else:
			get_node("SpriteBack").show()
			is_back_showing = true

	if ev.type == InputEvent.MOUSE_MOTION:
		if pressed:
			self.set_pos(ev.pos)
			for p_id in gamestate.players:
				rpc_id(p_id, "remote_piece_pos", ev.pos)

remote func remote_piece_pos(pos):
	regex.compile("([a-zA-Z_0-9 \\.\\(\\)])+") 
	regex.find(self.get_name())
	piece_name = regex.get_capture(0)
	get_parent().get_node(piece_name).set_pos(pos)

func _mouse_enter():
    enter = true

func _mouse_exit():
	if pressed == true:
		pressed = false
	enter = false
