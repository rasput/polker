extends Area2D

var base = "res://GamePack/" 
onready var game = gamepack.get_current_game_name()

var DEBUG = true
var click = false
var regex = RegEx.new()
var card_drawn = ""
var _deck_name = ""

func _ready():
	gamepack.connect("card_drawn",self,"_card_drawn")
	set_fixed_process(true)
	
func _fixed_process(delta):
	pass


func _input_event(viewport, ev, shape_idx):
	if ev.type == InputEvent.MOUSE_BUTTON and ev.button_index == BUTTON_RIGHT and ev.pressed == true and ev.is_pressed():
		if DEBUG:
			print("Deck right clicked")
		click = true
		add_card_from_deck(get_name())

func add_card_from_deck(deck_name):
	print("Adding from deck : " + str(deck_name))
	regex.compile("([a-zA-Z_0-9\\ \\.\\(\\)])+") 
	regex.find(deck_name)
	deck_name = regex.get_capture(0)
	_deck_name = deck_name
	if DEBUG:
		print("Drawing card from deck " + deck_name)
	gamepack.draw_card(deck_name)
	
func _card_drawn():
	if click:
		if DEBUG:
			print("Card Drawn on " + str(get_name()))
		card_drawn = gamepack.get_card()
		if DEBUG:
			print("card_drawn: " + card_drawn)
		if(card_drawn != "NoMoreCards"):
			var card = load("res://scenes/Game/Card.tscn").instance() 
			card.set_name(card_drawn)
			
			self.get_parent().add_child(card)
			card.set_pos(self.get_pos())
			card.deck_name = _deck_name
			card.get_node("SpriteBack").set_texture(load(base + game +"/Decks/"+ _deck_name +"/Back.png")) 
			card.get_node("Sprite").set_texture(load(base + game +"/Decks/"+ _deck_name +"/"+ card_drawn)) 
			for p_id in gamestate.players:
				rpc_id(p_id, "deal_card", _deck_name, card_drawn)

	click = false

remote func deal_card(deck, drawn_card):
	print("remotely drawing card on deck: " + deck + " for card " + drawn_card)
	var card = load("res://scenes/Game/Card.tscn").instance() 
	card.set_name(drawn_card)
	self.get_parent().add_child(card)
	
	
	card.get_node("Sprite").set_texture(load(base + game +"/Decks/"+ deck +"/"+ drawn_card)) 
