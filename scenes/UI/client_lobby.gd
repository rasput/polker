extends Panel

# Chat lobby UI logic
# author: rasput

func _ready():
	# Signals from gamestate tied to functions in the client lobby to tell client lobby scene what is happening
	# on the network
	print(gamepack.get_game_pack_list("res"))
	for game in gamepack.get_game_pack_list("res"):
		get_node("GridContainer/GridContainer1/OptionButton").add_item(game)
	gamestate.connect("connection_failed", self, "_on_connection_failed")
	gamestate.connect("connection_succeeded", self, "_on_connection_success")
	gamestate.connect("player_list_changed", self, "refresh_lobby")
	gamestate.connect("game_ended", self, "_on_game_ended")
	gamestate.connect("game_error", self, "_on_game_error")
	
	#Generate random name and colour for player
	get_node("GridContainer/GridContainer 3/PlayerNameInput").set_text(gamestate.get_player_name())
	randomize()
	get_node("GridContainer/GridContainer 3/ColorPickerButton").set_color(randi(255))
	# Set up the user interface for server and clients
	# We don't have a server started so checking with get_role()
	if gamestate.get_role() == "server":
		get_node("GridContainer/GridContainer/ConnectButton").hide();
		get_node("GridContainer/GridContainer/IPInput").set_editable(false)
		get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").add_text(":> " + "Starting server" )
		get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").newline()
		gamestate.host_game(get_node("GridContainer/GridContainer 3/PlayerNameInput").get_text())

	else:
		get_node("GridContainer/GridContainer/IPInput").set_editable(true)
		get_node("GridContainer/GridContainer/ServerButton").hide();
		get_node("GridContainer/GridContainer1/GameButton").hide();

# TODO This needs to be renamed, it's actually the "back" button to navigate to first scene
# and tear down the networking
func _on_HostButton_pressed():
	# Tear down network
	if gamestate.get_role() == "server" or get_tree().is_network_server():
		gamestate.end_game()
	else:
		print("client")
		get_tree().set_network_peer(null)
	
	# Go back to first screen
	get_node("/root/global").goto_scene("res://scenes/UI/entry_screen.tscn")

func _on_ConnectButton_pressed():
	gamestate.join_game(get_node("GridContainer/GridContainer/IPInput").get_text(), get_node("GridContainer/GridContainer 3/PlayerNameInput").get_text())

#TODO This needs to be renamed as the text input was repurposed to be the chat text entry
func _on_BroadcastPortInput1_text_entered( text ):
	_on_ChatSendButton_pressed()

func _on_ChatSendButton_pressed():
	var msg = get_node("GridContainer/GridContainer 2/Panel/ChatInput").get_text()
	if (msg == ""):
		return
	# Call the send_message proceedure remotely for every other 
	# and than the player who called it and the person who called it.
	rpc("send_message", get_node("GridContainer/GridContainer 3/PlayerNameInput").get_text() + "> " + msg)
	send_message( "(You):> " + msg)

remote func send_message(msg):
	get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").add_text( msg)
	get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").newline()
	get_node("GridContainer/GridContainer 2/Panel/ChatInput").set_text("")

func _on_MusicCheckBox_toggled( pressed ):
	pass # TODO replace with function body

func _on_SoundCheckBox_toggled( pressed ):
	pass # TODO replace with function body

# TODO add a network status indicator "light"
func _on_connection_success():
	get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").add_text(":> " + "Connected to server")
	get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").newline()

func _on_connection_failed():
	get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").add_text(":> " + "Failed to connect")
	get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").newline()

func _on_game_error(errtxt):
	get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").add_text(":> " + errtxt )
	get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").newline()

# TODO Need to test around this
func _on_game_ended():
	show()
#	get_node("connect").show()
#	get_node("players").hide()
#	get_node("connect/host").set_disabled(false)
#	get_node("connect/join").set_disabled(false)

# This is called when gamestate fires the "player_list_changed" signal
func refresh_lobby():
	var players = gamestate.get_player_list()
	players.sort()
	get_node("GridContainer/GridContainer1/PlayerList").clear()
	for p in players:
		print("player: " + str(p))
		if p == gamestate.get_player_name():
			get_node("GridContainer/GridContainer1/PlayerList").add_item(gamestate.get_player_name() + " (You)")
		else:
			get_node("GridContainer/GridContainer1/PlayerList").add_item(p)

func _on_ServerButton_pressed():
	get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").add_text(":> " + "Starting server" )
	get_node("GridContainer/GridContainer 2/Panel/RichTextLabel").newline()
	gamestate.host_game(get_node("GridContainer/GridContainer 3/PlayerNameInput").get_text())

func _on_GameButton_pressed():
	print(get_node("GridContainer/GridContainer1/OptionButton").get_selected())
	var option = get_node("GridContainer/GridContainer1/OptionButton")
	#gamepack.set_current_game(option.get_item_text(option.get_selected()))
	gamepack.rpc("set_current_game",option.get_item_text(option.get_selected()))
	gamestate.begin_game()


func _on_PlayerNameInput_text_entered( text ):
	gamestate.set_player_name(text)
	gamestate.name_change(get_tree().get_network_unique_id(), text)
	gamestate.rpc_id(1, "name_change", get_tree().get_network_unique_id(), text)
	#gamestate.name_change(get_tree().get_network_unique_id(), text)

func _on_OptionButton_item_selected( ID ):
	var option = get_node("GridContainer/GridContainer1/OptionButton")
	#gamepack.set_current_game(option.get_item_text(option.get_selected()))
	gamepack.rpc("set_current_game",option.get_item_text(option.get_selected()))
	rpc("_on_current_game_set", ID)

sync func _on_current_game_set(idx):
	 get_node("GridContainer/GridContainer1/OptionButton").select(idx)