extends Node

# author: rasput
#####################################################
####    Singleton used for gamestate management  ####
#####################################################
var role = "server"

# Default game port
const DEFAULT_PORT = 10567
# Max number of players
const MAX_PEERS = 12

# Name for my player
var player_name = "anon"
# Names for remote players in id:name format
var players = {}

# Signals to let lobby GUI know what's going on
signal player_list_changed()
signal connection_failed()
signal connection_succeeded()
signal game_ended()
signal game_error(what)

var world

func _ready():
	# Connect the network callbacks to the tree
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
func set_role(type):
	role = type
	if type == "server":
		player_name = "OP"
	else:
		randomize()
		player_name = "anon" + str(randi()%100)

func get_role():
	return role

func set_player_name(name):
	player_name = name

# Callback from SceneTree
func _player_connected(id):
	# This is not used in this, because _connected_ok is called for clients
	# on success and will do the job.
	pass

# Callback from SceneTree
func _player_disconnected(id):
	if (get_tree().is_network_server()):
		if (has_node("/root/Game/Table")): # Game is in progress
			emit_signal("game_error", "Player " + players[id] + " disconnected")
			end_game()
		else: # Game is not in progress
			# If we are the server, send to the new dude all the already registered players
			unregister_player(id)
			for p_id in players:
				# Erase in the server
				rpc_id(p_id, "unregister_player", id)

# Callback from SceneTree, only for clients (not server)
func _connected_ok():
	print("connected ok")
	# Registration of a client beings here, tell everyone that we are here
	rpc("register_player", get_tree().get_network_unique_id(), player_name)
	emit_signal("connection_succeeded")

# Callback from SceneTree, only for clients (not server)
func _server_disconnected():
	print("server disconnected")
	emit_signal("game_error", "Server disconnected")
	end_game()

# Callback from SceneTree, only for clients (not server)
func _connected_fail():
	print("connect fail")
	get_tree().set_network_peer(null) # Remove peer
	emit_signal("connection_failed")

# Lobby management functions
remote func name_change(id, name):
	players.erase(id)
	if (get_tree().is_network_server()):
		rpc("unregister_player", id)
		register_player(id, name)
		rpc("register_player", id, name)
	
remote func register_player(id, name):
	if (get_tree().is_network_server()):
		# If we are the server, let everyone know about the new player
		rpc_id(id, "register_player", 1, player_name) # Send myself to new dude
		for p_id in players: # Then, for each remote player
			rpc_id(id, "register_player", p_id, players[p_id]) # Send player to new dude
			rpc_id(p_id, "register_player", id, name) # Send new dude to player

	players[id] = name
	emit_signal("player_list_changed")

remote func unregister_player(id):
	players.erase(id)
	emit_signal("player_list_changed")

remote func pre_start_game(spawn_points):
	# Change scene
	#var world = load("res://scenes/Game/Table.tscn").instance()
	#get_tree().get_root().add_child(world)
	get_tree().get_root().get_node("lobby").hide()
	
	#et_node("/root/global").goto_scene("res://scenes/Game/Table.tscn")

	var player_scene = load("res://player.tscn")

	for p_id in spawn_points:
		var spawn_pos = world.get_node("Table/spawn_points" + str(spawn_points[p_id])).get_pos()
		var player = player_scene.instance()

		player.set_name(str(p_id)) # Use unique ID as node name
		player.set_pos(spawn_pos)

		if (p_id == get_tree().get_network_unique_id()):
			# If node for this peer id, set master
			player.set_network_mode(NETWORK_MODE_MASTER)
			player.set_player_name(player_name)
		else:
			# Otherwise set slave
			player.set_network_mode(NETWORK_MODE_SLAVE)
			player.set_player_name(players[p_id])

		world.get_node("Table/players").add_child(player)

	# Set up score
	# get_node("score").add_player(get_tree().get_network_unique_id(), player_name)
	#for pn in players:
	#	world.get_node("score").add_player(pn, players[pn])

	if (not get_tree().is_network_server()):
		# Tell server we are ready to start
		rpc_id(1, "ready_to_start", get_tree().get_network_unique_id())
	elif players.size() == 0:
		post_start_game()

remote func post_start_game():
	get_tree().set_pause(false) # Unpause and unleash the game!

var players_ready = []

remote func ready_to_start(id):
	assert(get_tree().is_network_server())

	if (not id in players_ready):
		players_ready.append(id)

	if (players_ready.size() == players.size()):
		for p in players:
			rpc_id(p, "post_start_game")
		post_start_game()

func host_game(name):
	player_name = name
	var host = NetworkedMultiplayerENet.new()
	host.create_server(DEFAULT_PORT, MAX_PEERS)
	get_tree().set_network_peer(host)

func join_game(ip, name):
	player_name = name
	var host = NetworkedMultiplayerENet.new()
	host.create_client(ip, DEFAULT_PORT)	
	get_tree().set_network_peer(host)

func get_player_list():
	return players.values()

func get_player_name():
	return player_name

func begin_game():
	assert(get_tree().is_network_server())
	
	#get_node("/root/global").goto_scene("res://scenes/Game/Game.tscn")
	get_node("/root/global").rpc("goto_scene", "res://scenes/Game/Game.tscn")
#	
#	pre_start_game(spawn_points)

func end_game():
	if (has_node("/root/scenes/Game/Table")): # Game is in progress
		# End it
		get_node("/root/scenes/Game/Table").queue_free()

	emit_signal("game_ended")
	players.clear()
	get_tree().set_network_peer(null) # End networking
