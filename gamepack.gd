extends Node
# author: rasput
#####################################################
####    Singleton used for gamepack management   ####
#####################################################
# Image format: PNG
# Image geometry:
# Dice, Piece:  64x64
# Deck, Card:   512x256
#
# Structure of game pack:
# 
# GameName(Folder) # Can be any name, will be used for the game name. Put this in the "GamePack" folder
# Contents:
# -> version.txt # contains only "v0.1" without the quotation marks
#
# -> Decks (Folder)
# -> Decks  -> Deck_nn (Folder)
# -> Decks  -> Deck_nn -> Back.png
# -> Decks  -> Deck_nn -> Card_nnn.png
#
# -> Die (Folder) # Dice faces need to be counted so keep seperate from pieces
# -> Die    -> Dice_nn (F)
# -> Die    -> Dice_nn -> Face_nn.png # Geometry 64x64 # nn corresponds to face number
#
# -> 2_sided_Pieces (Folder) # 2 sided pieces like credit chips with values needing hiding
# -> 2_sided_Pieces -> Piece_nnn (Folder) # Geometry 64x64
# -> 2_sided_Pieces -> Piece_nnn -> Piece_front_nn.png # nn corresponds to face value, if you want 3 pieces of same value, you'll have to put them in seperate Piece_nn folders
# -> 2_sided_Pieces -> Piece_nnn -> Piece_back_nn.png
#
# -> Pieces (Folder) # pawns, place holders, chips, credits, etc.
# -> Pieces -> Piece_nnn (Folder)
# -> Pieces -> Piece_nnn -> Back.png
# -> Pieces -> Piece_nnn -> Piece_nn.png
const DEBUG = true

var game_pack_directory = Globals.globalize_path("user://") + "GamePack"

var res_game_packs = []
var game_packs = [] # Keep track of the installed games

var object_list = [] # Game piece object types used Supported types [Decks, Pieces, 2_Sided_Pieces, Die]
var game_piece_instance_list = [] # Instances of decks or groups of pieces (These get added to board on begin_game or pre_game)
var image_list = [] # Image contents of decks and groups of pieces [Images of the faces, backs, and stacks/decks]

# TODO Remove these and use the variables above, this is duplication
var res_object_list = []
var res_image_list = []
var res_game_piece_instance_list = []

sync var random_number = -1
var current_game_name = ""

# Store a dictionary of cards in play, key'ed to their deck, draw_card can't draw a card that's already been drawn
# replace_card will use this to see if the card belongs to the deck and to remove if it does
var cards_in_play = {}
var card = ""
var card_list = []

var pieces_in_play = {}
var piece = ""
var piece_list = []

signal card_drawn
signal piece_drawn

func _ready():
	# Clean everything in initialization, 
	# TODO This could be optimized generating storing and using checksums then only perform this when necessary
	_mkdir("user", "root", "GamePack")
	_delete_game_pack_folder()
	_mkdir("res", "root", "GamePack")
	_parse_game_pack_dir(game_pack_directory)
	
func draw_card(deck_name):
	# Network code renames nodes from "card_name" to something like "@card_name@23"
	# I use node names for matching card image name to node, so filter out the added characters
	var regex = RegEx.new()
	regex.compile("([a-zA-Z_0-9 \\(\\)])+") 
	regex.find(deck_name)
	deck_name = regex.get_capture(0)
	if DEBUG:
		print ("regex name: " + regex.get_capture(0))
		
	card_list = get_image_list( current_game_name, "Decks", deck_name, "res")
	for c in card_list:
		print("Card: "+ c)
	set_random_number(card_list)
	card = card_list[random_number]
	
	while card == "Back.png" or cards_in_play.has(card): # Draw another card if the Back.png images is drawn
		var pcount = 0
		for c in cards_in_play:
			if cards_in_play[c] == deck_name:
				pcount = pcount + 1
		if (card_list.size() - 1) == pcount:
			pcount = 0
			return "NoMoreCards"
		
		set_random_number(card_list)
		card = card_list[random_number]
	
	cards_in_play[card] = deck_name
	rpc("set_client_card", deck_name, card)
	emit_signal("card_drawn")

sync func set_client_card(deck_name, card):
	self.card = card # This will work in python, will it work in gdscript?
	cards_in_play[card] = deck_name

func get_card():
	return card

func replace_card(card):
	if DEBUG:
		print("replacing card")
		print("cards in play: " + str(cards_in_play))
	cards_in_play.erase(card)
	
	if DEBUG:
		print("cards in play: " + str(cards_in_play))


func draw_piece(stack_name):
	# Network code renames nodes from "card_name" to something like "@card_name@23"
	# I use node names for matching card image name to node, so filter out the added characters
	var regex = RegEx.new()
	regex.compile("([a-zA-Z_0-9 \\(\\)])+") 
	regex.find(stack_name)
	stack_name = regex.get_capture(0)
	if DEBUG:
		print ("regex name: " + regex.get_capture(0))
	
	piece_list = get_image_list( current_game_name, "Pieces", stack_name, "res")
	for p in piece_list:
		print("Piece: "+ p)
	set_random_number(piece_list)
	piece = piece_list[random_number]
	
	while piece == "Back.png" or pieces_in_play.has(piece): # Draw another card if the Back.png images is drawn
		var pcount = 0
		for p in pieces_in_play:
			if pieces_in_play[p] == stack_name:
				pcount = pcount + 1
		if (piece_list.size() - 1) == pcount:
			pcount = 0
			return "NoMorePieces"
		
		set_random_number(piece_list)
		piece = piece_list[random_number]
	
	pieces_in_play[card] = stack_name
	rpc("set_client_piece", stack_name, piece)
	emit_signal("piece_drawn")

sync func set_client_piece(stack_name, piece):
	self.piece = piece
	pieces_in_play[piece] = stack_name

func get_piece():
	return self.piece

func replace_piece(piece):
	if DEBUG:
		print("replacing piece")
		print("pieces in play: " + str(pieces_in_play))
	pieces_in_play.erase(piece)
	
	if DEBUG:
		print("pieces in play: " + str(pieces_in_play))


func set_random_number(list):
	randomize()
	random_number = randi()%list.size() # Random number between 0 and number of cards in deck
	rset("random_number", random_number)

sync func set_current_game(game_name):
	current_game_name = game_name

func get_current_game_name():
	if DEBUG:
		print("getting current game: " + current_game_name)
	return current_game_name
	
# Creates list of the game pack folder
func get_game_pack_list(res="user"):
	if DEBUG:
		print("generating list of game packs")
	if res == "user":
		game_packs = _dir_list(game_pack_directory)
		return game_packs
	elif res == "res":
		res_game_packs = _dir_list(Globals.globalize_path("res://")+"GamePack/")
		return res_game_packs
	else:
		return "Invalid resource requested"

# Creates a list of the top level folders in a game pack. i.e. The object types used in game
func get_object_list(game, res="user"):
	if DEBUG:
		print("generating list of game objects: " + game)
	if res == "user":
		object_list = _dir_list(game_pack_directory + "/" + game)
		return object_list
	elif res == "res":
		res_object_list = _dir_list(Globals.globalize_path("res://") + "GamePack/" + game)
		return res_object_list
	else:
		return "Invalid resource requested"

func get_instance_list(game, obj, res="user"):
	if DEBUG:
		print("Getting instance list " + res)
	if res == "user":
		game_piece_instance_list = _dir_list(game_pack_directory + "/" + game + "/" + obj)
		return game_piece_instance_list
	elif res == "res":
		res_game_piece_instance_list = _dir_list(Globals.globalize_path("res://") + "GamePack/" + game + "/" + obj)
		return res_game_piece_instance_list
	else:
		return "Invalid resource requested"

func get_image_list(game, obj, instance, res="user"):
	if res == "user":
		image_list = _dir_list(game_pack_directory + "/" + game + "/" + obj + "/" + instance, true)
		return image_list
	elif res == "res":
		res_image_list = _dir_list(Globals.globalize_path("res://") + "GamePack/" + game + "/" + obj + "/" + instance, true)
		return res_image_list
	else:
		return "Invalid resource requested"

# helper function returns txt and png filenames if list_files is true, otherwise returns folders
func _dir_list(dir, list_files=false):
	if DEBUG:
		print("_dir_list: " + dir)
	var _dir_list = []
	var _dir = Directory.new()
	_dir.open(dir)
	_dir.list_dir_begin()
	while true:
		var _dir_entry = _dir.get_next()
		if list_files == false:
			if _dir_entry == "":
				break
			elif not _dir_entry.begins_with(".") and not _dir_entry.ends_with(".txt") and not _dir_entry.ends_with(".png"):
				_dir_list.append(_dir_entry)
		elif list_files == true:
			if _dir_entry == "":
				break
			elif not _dir_entry.begins_with(".") and ( _dir_entry.ends_with(".txt") or _dir_entry.ends_with(".png") ):
				_dir_list.append(_dir_entry)
	
	_dir.list_dir_end()
	return _dir_list

# Helper function, makes directory inside base. Makes folder in user root when base="user"
# Passing true will remove a pre-existing folder of same name before creation
func _mkdir(res="res", base="root", dir=""):
	var _dir = Directory.new()
	var _open = ""
	var _folder = ""
	
	if res == "res":
		_open =Globals.globalize_path("res://") 
	elif res == "user":
		_open =Globals.globalize_path("user://")
	else:
		if DEBUG:
			print("Invalid resource entered")
		return

	if base != "root":
		_open = _open + "/" + base + "/"

	if DEBUG:
		print("_mkdir: open folder: " + _open )
	_dir.open(_open)
	if DEBUG:
		print("_mkdir: creating folder: " + dir)
	_dir.make_dir(dir)

# Helper function to copy files from user:// to res://
func _copy( base, game, obj, inst, img):
	var _dir = Directory.new()
	var d = Directory.new()
	_dir.open(Globals.globalize_path("res://") + base + "/" + game + "/" + obj + "/" + inst)
	#_dir.open("res://" + base + "/" + game + "/" + obj + "/" + inst)
	_dir.copy(Globals.globalize_path("user://") + base + "/" + game + "/" + obj + "/" + inst + "/" + img, Globals.globalize_path("res://") +  base + "/" +game + "/" + obj + "/" + inst + "/" + img)

func _delete_game_pack_folder():
	var _dir = Directory.new()
	
	for game in get_game_pack_list("res"):
		for obj in get_object_list(game,"res"):
			for inst in get_instance_list(game, obj,"res"):
				for img in get_image_list(game, obj, inst,"res"):
					_dir.remove(Globals.globalize_path("res://") + "GamePack/" + game + "/"+ obj + "/"+ inst +"/" + img)

	for game in get_game_pack_list("res"):
		for obj in get_object_list(game,"res"):
			for inst in get_instance_list(game, obj,"res"):
				_dir.remove(Globals.globalize_path("res://") + "GamePack/" + game + "/"+ obj + "/"+ inst)

	for game in get_game_pack_list("res"):
		for obj in get_object_list(game,"res"):
			_dir.remove(Globals.globalize_path("res://") + "GamePack/" + game + "/"+ obj)

	for game in get_game_pack_list("res"):
		_dir.remove(Globals.globalize_path("res://") + "GamePack/" + game)
	_dir.remove(Globals.globalize_path("res://") + "GamePack")

# Generates the list of game_packs installed and stores them, do this on start.
func _parse_game_pack_dir(dir):
	for game in get_game_pack_list():
		_mkdir("res", "GamePack", game)
		if DEBUG:
			print ("Parsing game: " + game)
		_import_game_pack(game)

# Copies user://GamePacks, creates folder structure and 
# import the images to be used in the game engine's res://
func _import_game_pack(game):
	for obj in get_object_list(game):
		_mkdir("res", "GamePack/" + game, obj)
		for inst in get_instance_list(game, obj):
			_mkdir("res", "GamePack/" + game + "/" + obj, inst)
			for img in get_image_list(game, obj, inst):
				_copy("GamePack", game, obj, inst, img )